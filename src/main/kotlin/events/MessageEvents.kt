package events

import com.natpryce.konfig.Key
import com.natpryce.konfig.booleanType
import com.natpryce.konfig.stringType
import config
import net.dv8tion.jda.core.entities.ChannelType
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter

class MessageEvents : ListenerAdapter() {
    private val profanityFilter = Key("profanity-filter", booleanType)
    private val swearWords = Key("profanity-filter-string", stringType)
    private val prefix = Key("prefix", stringType)
    override fun onMessageReceived(event: MessageReceivedEvent?) {
        if (event!!.isFromType(ChannelType.PRIVATE)) return
        if (event.member.user.isBot) return
        if (event.message.contentRaw.startsWith(config[prefix])) return
        if (config[profanityFilter]) {
            val swearWordsRegex = config[swearWords].toRegex(RegexOption.IGNORE_CASE)
            if (swearWordsRegex.containsMatchIn(event.message.contentDisplay)) {
                event.message.delete().queue()
                event.message.channel.sendMessage("Last message by ${event.message.author.name} removed by StiwyBot for using mature language")
                        .queue()
            }
        }

    }
}