package events

import net.dv8tion.jda.core.events.channel.text.TextChannelCreateEvent
import net.dv8tion.jda.core.events.channel.text.TextChannelDeleteEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter

class ChannelEvents : ListenerAdapter() {
    override fun onTextChannelCreate(event: TextChannelCreateEvent?) {
        event!!.channel.sendMessage("Welcome to your new channel").queue()
    }

    override fun onTextChannelDelete(event: TextChannelDeleteEvent?) {
        println("A text channel '${event!!.channel.name}' was deleted")
    }
}