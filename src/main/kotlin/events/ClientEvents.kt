package events

import net.dv8tion.jda.core.events.DisconnectEvent
import net.dv8tion.jda.core.events.ReadyEvent
import net.dv8tion.jda.core.events.Event
import net.dv8tion.jda.core.events.ReconnectedEvent
import net.dv8tion.jda.core.hooks.EventListener

class ClientEvents : EventListener {

    override fun onEvent(event: Event) {
        if (event is ReadyEvent)
            println("Logged in as ${event.jda.selfUser.name} - (${event.jda.selfUser.id})")
        if (event is DisconnectEvent)
            println("You have been disconnected!")
        if (event is ReconnectedEvent)
            println("Reconnected!")
    }
}