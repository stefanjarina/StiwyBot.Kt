package commands

import com.natpryce.konfig.Key
import com.natpryce.konfig.stringType
import config
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter

class OnlineCommand : ListenerAdapter() {
    private val prefix = Key("prefix", stringType)
    override fun onGuildMessageReceived(event: GuildMessageReceivedEvent?) {
        if (event!!.message.contentRaw.startsWith(config[prefix], ignoreCase = true)) {
            event.channel.sendMessage("There are ${event.guild.members.size} members")
        }
    }
}